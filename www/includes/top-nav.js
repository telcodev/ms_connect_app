
function render_top_menu(back_page, back_icon='ion-ios-arrow-thin-left')
{
    if (!back_page){

        document.write(`


               <div class="w-nav-button navbar-button left" id="menu-button" data-ix="hide-navbar-icons">
                            <div class="navbar-button-icon home-icon">
                                <div class="bar-home-icon top"></div>
                                <div class="bar-home-icon middle"></div>
                                <div class="bar-home-icon bottom"></div>
                            </div>
                        </div>

               `);

        } else {

            document.write(`


               <div class="w-nav-button navbar-button left" id="menu-button" data-ix="hide-navbar-icons">
                            <div class="navbar-button-icon home-icon">
                                <div class="bar-home-icon top"></div>
                                <div class="bar-home-icon middle"></div>
                                <div class="bar-home-icon bottom"></div>
                            </div>
                        </div>
                        <a class="w-inline-block navbar-button right" href="`+back_page+`" data-loader="1">
                            <div class="navbar-button-icon icon `+ back_icon +`"></div>
                        </a>


               `);

        }

}
