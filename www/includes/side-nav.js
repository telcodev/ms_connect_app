document.write(`


               <nav class="w-nav-menu nav-menu bg-gradient" role="navigation">
                            <div class="nav-menu-header">
                                <div class="logo">MS Connect</div>
                            </div>
                            <a class="w-clearfix w-inline-block nav-menu-link" href="home.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-home-outline"></div>
                                </div>
                                <div class="nav-menu-titles">MS Home</div>
                            </a>
                            <a class="w-clearfix w-inline-block nav-menu-link" href="docs.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-box-outline"></div>
                                </div>
                                <div class="nav-menu-titles">MS Sample Agreements</div>
                                <div class="nav-menu-text-right">4</div>
                            </a>
                            <a class="w-clearfix w-inline-block nav-menu-link" href="news.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-paper-outline"></div>
                                </div>
                                <div class="nav-menu-titles">MS Latest News</div>
                            </a>
                            <a class="w-clearfix w-inline-block nav-menu-link" href="calendar.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-calendar-outline"></div>
                                </div>
                                <div class="nav-menu-titles">MS Meetings</div>
                            </a>
                            <a class="w-clearfix w-inline-block nav-menu-link" href="gallery.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-people"></div>
                                </div>
                                <div class="nav-menu-titles">MS Expertise</div>
                            </a>
                            <a class="w-clearfix w-inline-block nav-menu-link" href="contact.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-at-outline"></div>
                                </div>
                                <div class="nav-menu-titles">MS Contact</div>
                            </a>
                            <a class="w-clearfix w-inline-block nav-menu-link" href="settings.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-gear-outline"></div>
                                </div>
                                <div class="nav-menu-titles">Settings</div>
                            </a>
                            <a class="w-clearfix w-inline-block nav-menu-link last" href="terms.html" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-ios-bookmarks-outline"></div>
                                </div>
                                <div class="nav-menu-titles">Terms and Conditions</div>
                            </a>
                            <a id="log-out" class="w-clearfix w-inline-block nav-menu-link last" href="#" data-load="1">
                                <div class="icon-list-menu">
                                    <div class="icon ion-log-out"></div>
                                </div>
                                <div class="nav-menu-titles">Log out</div>
                            </a>
                            <div class="separator-bottom"></div>
                            <div class="separator-bottom"></div>
                            <div class="separator-bottom"></div>
                        </nav>

                        <div class="wrapper-mask" data-ix="menu-mask"></div>


               `);
