// localStorage validator
if (!localStorage.getItem('user' || !localStorage.getItem('role'))){
    firebase.auth().signOut()
}
// auth observer
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        // session still runs
    } else {
        // no one is signed in.
        alert('Your session has been terminated, we will redirect you to login.');
        window.location = 'login.html';
    }
});
