var populated = 0;
var ola = localStorage.getItem('offline_articles') || '[]';
var querySnapshot = JSON.parse(ola);

if (querySnapshot.length > 0)
$('.w-slider-mask').html('');

querySnapshot.forEach(function(data) {

  if (!data['image_url'] || populated >= 3)
  return;

  var article = `

  <div class="w-slide slide bg" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(`+data['image_url']+`);"><a href="news_article.html#`+data['id']+`"><div class="slider-element-test">`+ data["title"].substring(0, 15) +`<br>`+ data["article"].substring(0, 40) +`...<br><small><i>Read More...</i></small></div></a></div>
  `;
  $('.w-slider-mask').append(article);
  populated++;
});
