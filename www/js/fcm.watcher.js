document.addEventListener('deviceready', setupFCM);


function setupFCM() {
  // Stuff to do as soon as the script is ready
  var old_fcm_token = localStorage.getItem('fcm_token') || '';

  FCMPlugin.getToken(function(token){

    if (old_fcm_token != token || old_fcm_token == ''){
      user = localStorage.getItem('user');
      db.collection("user_metadata").doc(user).update({
        fcm_token: token
      }).then(function() {
        localStorage.setItem('fcm_token', token);
        console.log("Token successfully updated! " + token);
      })
      .catch(function(error) {
        console.log("Error updating token: ", error);
      });
    }

  });



  FCMPlugin.onTokenRefresh(function(token){
    user = localStorage.getItem('user');
    db.collection("user_metadata").doc(user).update({
      fcm_token: token
    }).then(function() {
      localStorage.setItem('fcm_token', token);
      console.log("Token successfully updated! " + token);
    })
    .catch(function(error) {
      console.log("Error updating token: ", error);
    });
  });

}
