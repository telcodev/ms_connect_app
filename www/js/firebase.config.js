// Initialize Firebase
  var config = {
    apiKey: "AIzaSyBgun781NYOAzKVhVI72qAcUk_1dXfgVhM",
    authDomain: "ms-connect-app.firebaseapp.com",
    databaseURL: "https://ms-connect-app.firebaseio.com",
    projectId: "ms-connect-app",
    storageBucket: "ms-connect-app.appspot.com",
    messagingSenderId: "589659740545"
  };
  firebase.initializeApp(config);
  var db = firebase.firestore();

  // Sign in using a redirect.
firebase.auth().getRedirectResult().then(function(result) {
   if (result.credential) {
      // This gives you a Google Access Token.
      var token = result.credential.accessToken;
      console.log('TOKEN', token);
   }
   var user = result.user;
   console.log('USER', user);
});
