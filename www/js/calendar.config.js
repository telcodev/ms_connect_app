// Client ID and API key from the Developer Console
var CLIENT_ID = '589659740545-k34nt7tv593a4c19pofdp4qmvn1cvt6p.apps.googleusercontent.com';
var API_KEY = 'AIzaSyCPFK3Mf3OFYfAQnJ4rlNLqguZz5Q28_8g';
var CALENDAR_ID = 'non8kcbv8fuuu0a322hapkvv64@group.calendar.google.com';
var userEvents = [];

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = "https://www.googleapis.com/auth/calendar";

var DONE_LOADING = false;

var authorizeButton = document.getElementById('authorize-button');
var signoutButton = document.getElementById('signout-button');

/**
*  On load, called to load the auth2 library and API client library.
*/
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
*  Initializes the API client library and sets up sign-in state
*  listeners.
*/
function initClient() {
  gapi.client.init({
    apiKey: API_KEY,
    clientId: CLIENT_ID,
    discoveryDocs: DISCOVERY_DOCS,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
    grantWritetoUser();
    getUserEvents();

  });
}
function grantWritetoUser(){
  usr = localStorage.getItem('user');
  var req = {
    "calendarId": CALENDAR_ID,
    "resource": {
      "role": "writer",
      "scope": {
        "type": "user",
        "value": usr
      }
    }
  }
  var request = gapi.client.calendar.acl.insert(req);
  request.execute(function(resp) {
    console.log('PERMIT: ', resp);
  });
}

function getUserEvents(){
  var request = gapi.client.calendar.events.list({
    'calendarId': CALENDAR_ID,
    //'q': localStorage.getItem('user')
  });

  request.execute(function(resp){
    console.log('EVENTS: ', resp);
  });
}

/**
*  Called when the signed in status changes, to update the UI
*  appropriately. After a sign-in, the API is called.
*/
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    console.log('we are signed in');
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
  }
  DONE_LOADING = true;
  $('#new-stack').addClass('stop-loading');
}

/**
*  Sign in the user upon button click.
*/
function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn();
}

/**
*  Sign out the user upon button click.
*/
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();
}

/**
* Append a pre element to the body containing the given message
* as its text node. Used to display the results of the API call.
*
* @param {string} message Text to be placed in pre element.
*/
